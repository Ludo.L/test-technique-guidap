import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    articles: [],
    loading: true,
  },

  mutations: {
    saveNewArticle(state, article) {
      const id = state.articles.length + 1;
      const newArticle = {
        title: article.title,
        body: article.body,
        id,
      };
      state.articles.push(newArticle);
    },

    saveModifiedArticle(state, { title, body, id }) {
      const ModifiedArticle = {
        title,
        body,
        id,
      };
      state.articles.splice(id - 1, 1, ModifiedArticle);
    },

    updatePosts(state, articles) {
      if (state.articles.length === 0) {
        state.articles = articles;
      }
    },
    changeLoadingState(state, loading) {
      state.loading = loading;
    },
  },

  actions: {
    loadData({ commit }) {
      axios.get('https://jsonplaceholder.typicode.com/posts').then((response) => {
        commit('updatePosts', response.data);
        commit('changeLoadingState', false);
      });
    },
  },

  getters: {
    articles(state) {
      return state.articles;
    },
    articleId(state) {
      return state.articles.id;
    },
  },

});
